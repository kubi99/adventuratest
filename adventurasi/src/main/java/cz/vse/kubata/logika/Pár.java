
package cz.vse.kubata.logika;





/*******************************************************************************
 * Tato třída Pár slouží k přenášení věci a seznamu věcí (řetězec).
 * Využívá se u třídy Postava.
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-15
 */
 public class Pár
   {


    private Vec vracenaVec;
    private String vracenyString;


    /**
     * Konstruktor
     * 
     * @param vracenaVec, kterou postava vrací(dává).
     *  seznam věcí, který postava nosí.
     */
    public Pár(Vec vracenaVec, String vracenyString)
    {
        this.vracenaVec = vracenaVec;
        this.vracenyString = vracenyString;
    }

    /**
     * Getter na vrácenou věc.
     * 
     * @return věc, ktrou pár nese.
     */
    public Vec getVracenaVec()
    {
        
        return vracenaVec;
    }

    /**
     * Getter na vrácený seznam (řetězec).
     * 
     * @return seznam, který pár nese.
     */
    public String getVracenyString(){
        return vracenyString;
    }
}
