
package cz.vse.kubata.logika;





/*******************************************************************************
 * Instance třídy PrikazVykopej implementuje do hry příkaz nebo možnost
 * vykopat věc z jiné věci.
 * 
 *
 * @author  Jan Kubata 
 * @version 1.00.0000 — 2020-06-15
 */
public class PrikazVykopej implements IPrikaz 
{

    private static final String NAZEV = "vykopej";
    private HerniPlan plan;
    
    /**
     * Konstruktor
     * 
     * @param plan, plán (možnost vykopání věcí)
     */
    public PrikazVykopej(HerniPlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda pro provedení příkazu ve hře.
     * 
     * @param parametry, jeden parametr na určení věci, kterou chce uživatel vykopat
     */
    public String provedPrikaz(String... parametry){
        String odpoved = "";
        if(parametry.length == 0){
            odpoved = "Neřekl jsi, co je potřeba vykopat!";
        }
        else{
            String nazevVeci = parametry[0];
            Prostor aktualni = plan.getAktualniProstor();
            if(aktualni.jeVecVProstoru(nazevVeci)){
                Vec hrob = aktualni.vratVec(nazevVeci);
                if(hrob.isLzeVykopat()){
                    Batoh batoh = plan.getBatoh();
                    if(batoh.obsahujeVec("lopata")){
                        hrob.setVykopana(true);
                        return "V hrobě jsi našel starou dýku. Nejspiš patřila zakopanému nebožtíkovi.\n"+hrob.vratObsahVeci();
                    }
                    else{
                        odpoved = "S tím co máš u sebe, hrob určitě nevykopeš!";
                    }
                }
                else{
                    odpoved = "Tato věc nejde vykopat.";
                }
                
            }
            else{
                odpoved = "Tato věc tu není.";
            }
            
            
        }
        
        return odpoved;
    }


    /**
     * Metoda vrací název příkazu - slovo pro vyvolání příkazu.
     * 
     * @return název příkazu
     */
    public String getNazev(){
        return NAZEV;
    }
}
