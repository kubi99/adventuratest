
package cz.vse.kubata.logika;


import java.util.Map;
import java.util.HashMap;


/*******************************************************************************
 * Instance třídy Věc představují věci, které můžeme najiít v prostorách hry,
 * popřípadě v jiných věcech nebo u postav.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-15
 */
public class Vec
{
    private String nazev;
    private boolean prenositelna;
    private boolean lzeVykopat = false;
    private boolean vykopana = false;
    private boolean zbran = false;
    private boolean lzeOdemknout = false;
    private boolean odemknuta = false;
    private Map<String,Vec> obsahVeci;
    /**
     * Konstruktor vytvářející jednotlivé věci. Určuje jestli jsou přenostitelné
     * a jak se jmenují. A také vytvoří mapu pro věci, které b mohla daná věc 
     * obsahovat, např.: trezor obsahuje peníze.
     * 
     * @param prenositelna věci
     * @param nazev, true - lze přenést, false - nelze přenést
     *
     */
    public Vec(String nazev, boolean prenositelna)
    {
        this.nazev = nazev;
        this.prenositelna = prenositelna;
        obsahVeci = new HashMap<String,Vec>();
    }

    /**
     * Nastavuje možnost vykopání věci. Poku je věc vykopatelná, lze vykopat.
     * 
     * @param lzeVykopat(boolean) hodnota možnosti vykopání, true - lze vykopat, false - nelze
     */
    public void setLzeVykopat(boolean lzeVykopat){
        this.lzeVykopat = lzeVykopat;
    }
    
    /**
     * Nastavuje jestli je věc vykopaná.
     * 
     * @param vykopana-boolean hodnota vykopání.
     */
    public void setVykopana(boolean vykopana){
        this.vykopana = vykopana;
    }
    
    /**
     * Vrací údaj o možnosti věc vykopat.
     * 
     * @return boolean hodnota možnosti vykopání.
     */
    public boolean isLzeVykopat(){
        return lzeVykopat;
    }
    
    /**
     * Vrací údaj jestli je věc vykopaná.
     * 
     * @return boolean hodnota vykopání.
     */
    public boolean isVykopana(){
        return vykopana;
    }
    
    
    /**
     * Nastavuje možnost odemknutí věci. Poku je věc odemknutelná, lze odemnkout.
     * 
     * @param lzeOdemknout, boolean hodnota možnosti odemknutí.
     */
    public void setLzeOdemknout(boolean lzeOdemknout){
        this.lzeOdemknout = lzeOdemknout;
    }
    
    /**
     * Nastavuje jestli je věc odemčená.
     * 
     * @param odemknuta, boolean hodnota odemčení.
     */
    public void setOdemknuta(boolean odemknuta){
        this.odemknuta = odemknuta;
    }
    
    /**
     * Vrací údaj o možnosti věc odemknout.
     * 
     * @return boolean hodnota možnosti odemknutí.
     */
    public boolean isLzeOdemknout(){
        return lzeOdemknout;
    }
    
    /**
     * Vrací údaj jestli je věc odemčená.
     * 
     * @return boolean hodnota odemčení.
     */
    public boolean isOdemknuta(){
        return odemknuta;
    }
    
    /**
     * Vrátí seznam věcí, které daná věc obsahuje.
     * 
     * @return seznam věcí, které věc obsahuje.
     */
    public String vratObsahVeci(){
        String obsah = "";
        for (String nazev : obsahVeci.keySet()){
            obsah+=" "+nazev;
        }
        return obsah;
    }
    
    /**
     * Vrací údaj, jestli věc obsahuje jinou věc, podle názvu věci, kterou hledáme.
     * 
     * @param jmeno, název hledané věci
     * @return boolean hodnota, zda-li věc obsahuje jinou věc a zároveň je odemčená,
     * díky tomu se může uživatel k hledané věco dostat. 
     */
    public boolean obsahujeVecTutoVec(String jmeno){
        return (odemknuta | vykopana) && obsahVeci.containsKey(jmeno);
    }
    
    /**
     * Vloží věc do jiné věci, poku lze věc odemknout.
     * 
     * @param vec, vkládaná věc do jiné věci.
     */
    public void vlozVecDoVeci(Vec vec){
        if(this.lzeOdemknout | this.lzeVykopat){
            obsahVeci.put(vec.getNazev(), vec);
        }
        
        
    }
    
    /**
     * Odebere věc z jiné věci podle názvu věci, kterou chceme odebrat.
     * 
     * @param nazevVeci, název věci, kterou cheme odebrat.
     * @return odebraná věc.
     */
    public Vec odeberVecZVeci(String nazevVeci){
        Vec sebirana = null;
        if ((odemknuta | vykopana)&& obsahVeci.containsKey(nazevVeci)){
            sebirana = obsahVeci.get(nazevVeci);
            if(sebirana.isPrenositelna()){
                obsahVeci.remove(nazevVeci);
            }
        }
        return sebirana;
    }
    
    /**
     * Vrátí název určité věci.
     * 
     * @return název věci.
     */
    public String getNazev(){
        return nazev;
    }
    
    /**
     * Getter na přenositelnost.
     * 
     * @return název věci.
     */
    public boolean isPrenositelna(){
        return prenositelna;
    }
    
    /**
     * Nastavuje jestli je věc přenositelná - lze sebrat a přenášet.
     * 
     * @param prenositelna, boolean hodnota přenositelnosti
     */
    public void setPrenositelna(boolean prenositelna){
        this.prenositelna = prenositelna;
    }
    
    /**
     * Getter na věc, jestli je zbraň.
     * 
     * @return boolean hodnota zda je věc zbraň.
     */
    public boolean isZbran(){
        return zbran;
    }
    
    /**
     * Nastaví o věci, že je zbraň.
     * 
     * @param zbran, boolean hodnota zda je věc zbraň.
     */
    public void setZbran(boolean zbran){
        this.zbran = zbran;
    }
}
